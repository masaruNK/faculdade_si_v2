/*
Aluno: Kaio M. Nakazono  N�USP: 5617059
- OBS:
1) Split duplo (divisao pagina interna) n�o funciona.
2) Remocao apenas no caso simples: quantidade elementos da pagina > t-1.
3) Deve ter outos bugs, infelizmente.
4) Materiais consultados:
    - Livro "Projeto de Algoritmos", autor: Ziviani, cap.6
    - Slides da aula 15 e 16 de AED II - prof. Ariane
    - Slides:   http://marciobueno.com/arquivos/ensino/ed2/ED2_04_Arvore_B+.pdf
                http://www.di.ufpb.br/lucidio/Btrees.pdf
*/

#include <stdio.h>
#include <stdlib.h>

#define t 3
#define true 1
#define false 0

typedef struct pagina {
  int n;
  int folha;
  int chaves[2 * t - 1];
  struct pagina *filho[2 * t];
} Pagina, Btree;

Btree* Inicializa(){
    return NULL;
}

Btree* CriaBtree () {
    int i;
    Btree *a = (Btree *)malloc(sizeof(Btree));
    a->n = 0;
    a->folha = true;
    return a;
}

/*teste impressao*/
/*
void imprimeI3(Btree* b, int nivel){
    int i;
    if(b == NULL)return;
    printf("Nivel %d: ",nivel);
    for(i = 0; i < b->n; i++){
        //printf("i = %d\n",i);
        printf("%d ",b->chaves[i]);
    }
    printf("\n");
    nivel++;
    for(i = 0; i <= b->n; i++){
        if(b->folha == false){
            imprimeI3(b->filho[i],nivel);
        }
    }
}

void imprime3(Btree* b){
    int n = 0;
    if(b == NULL){
        printf("Vazia\n");
        return;
    }
    else{
      imprimeI3(b,n);
    }

}
*/

/*Imprime Pre-Ordem*/
void imprimeRecursiva(Btree* b, FILE *fileTemp){

    if(b == NULL)return;
    if(fileTemp == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");
    int i;
    fprintf(fileTemp,"(");
    for(i = 0; i <= b->n; i++){
        if(b->folha == false){
            imprimeRecursiva(b->filho[i],fileTemp); //chamada recursiva para o filho

            //se nao tiver essa condicao, vai imprimir lixo no final poi i == b->n, mas nesse caso, i deve ser  < b->n
            if(i < b->n){
                fprintf(fileTemp,"%d ",b->chaves[i]); //quando retorna a raiz imprime a chave da raiz
            }
        }
        if(b->folha == true){
            for(i = 0; i < b->n; i++){
                fprintf(fileTemp,"%d ",b->chaves[i]); //escreve no arquivo as chaves das folhas
            }
        }
    }
    fprintf(fileTemp,")");
}

/*Passo como parametro o nome do arquivo de saida e adiciono o print nesse arquivo*/
void imprimePreOrdem(Btree *b, char *nomeSaida){
    FILE *fileTemp;
    fileTemp = fopen(nomeSaida,"a"); //'a' de append

    if(b == NULL){
        return;
    }
    else{
      imprimeRecursiva(b,fileTemp);
    }
    fprintf(fileTemp,"\n");
}

/*Divide a pagina cheia, copiando o elemento da mediana e os seguintes para uma nova pagina*/
Btree* splitPagina (Btree *x, int i, Btree *y) {
    int j;
    Btree *z = CriaBtree(); //cria nova pagina folha para alocar elementos de overflow
    //printf("@ endereco de z: %p\n",&z);


    //copio elemento mediano da pagina y e os seguintes para a nova pagina z
    int indice = t-1; // = 2, elemento mediano da pagina y
    for (j = 0; j < t ; j++){
        z->chaves[j] = y->chaves[indice];
        z->n++;
        indice++;
    }

    //se pagina cheia nao for folha, devo ajustar os ponteiros para os filhos
    if (y->folha == false)
    {
        for (j = 0; j < t; j++){
            z->filho[j] = y->filho[indice];
            indice++;
        }
    }

    y->n = t - 1;
   // printf("!! x->n = %d\n",x->n);

    //primeiro split: j = 0, pois x->n = 0, nao entra aqui
    for (j = x->n; j >= i+1; j--){
        x->filho[j+1] = x->filho[j];
         printf("* j = %d -- x->filho[%d] = %d\n",j,j+1,x->filho[j]);
    }

    //aponta para endereco de z
    x->filho[i+1] = z;
    //printf("#x->filho[%d] = %p\n",i+1,&z);

    //tmb, primeiro split nao entra aqui, x->n = 0
    for (j = x->n-1; j >= i; j--){
        x->chaves[j+1] = x->chaves[j];
        //printf("$$ x->chaves[%d] = %d\n",j+1,x->chaves[j]);
    }

    x->chaves[i] = y->chaves[t-1];
   // printf("++ x->chaves[%d] = %d\n",i,x->chaves[i]);

    x->n++;
    //printf("!! saindo do split: x->n = %d\n",x->n);

   return x;
}


Btree* insereNaoCheio (Btree *x, int k) {
    int i = x->n-1;
    //printf("!!! i = %d\n",i);
    if (x->folha == true){ // folha z

        while (i >= 0 && x->chaves[i] > k){
            x->chaves[i+1] = x->chaves[i];
            i--;
        }
        x->chaves[i+1] = k;
        x->n++;
    }
    else
    {
        while (i >= 0 && x->chaves[i] > k){
            i--;
        }

        i = i + 1;

        if (x->filho[i]->n == 2*t-1){
            x = splitPagina(x, i, x->filho[i]);

            if (k > x->chaves[i]){
                i = i + 1;
            }
        }

        x->filho[i] = insereNaoCheio(x->filho[i], k);
    }
    return x;
}

/*Insere elementos na pagina*/
Btree *insere (Btree *root, int chaves) {
   Btree *r = root;
   if (r->n == (2*t - 1)) { //pagina cheia
      Btree *s = CriaBtree(); //crio uma nova pagina
      s->folha = false; //nao eh folha, sera pagina indice
      s->filho[0] = r; //aponta para r, referencia para root
      s = splitPagina (s, 0, r); //divide a pagina r (cheia) e retorna
      s = insereNaoCheio (s, chaves);
      return s;
   }
   else {
      return insereNaoCheio (r, chaves);
   }
}

/*Pesquisa ch e ja o remove - somente caso simples*/
void pesquisaRemove(Btree *p, int ch){
    if(p == NULL){
        //printf("Arvore vazia\n");
        return;
    }
    int i = 1;

    //se pagina interna, percorro ate seu indice
    if(p->folha == false){
        while(i < p->n && ch > p->chaves[i-1]){
            i++;
        }
    }
    //se pagina folha, percorro todas as suas chaves
    if(p->folha == true){
         while(i < p->n && ch > p->chaves[i-1]){
            i++;
        }
        //achou chave
        if(p->folha && ch == p->chaves[i-1]){
            int j;
            //printf("Achou chave - posicao: %d !!!\n",i);
            //verifico se pagina tem a quantidade minima de elementos, delecao segura
            if(p->n > t-1){
                for(j = i - 1; j < p->n; j++){
                    p->chaves[j] = p->chaves[j+1];
                }
                p->n--;
            }
            return;
        }
    }
    //ch nao esta nessa pagina, procura a esquerda
    if(ch < p->chaves[i-1]){
        pesquisaRemove(p->filho[i-1],ch);
    }
    //ou procuro a direita
    else{
        pesquisaRemove(p->filho[i],ch);
    }
}

/*Funcao que recebe o nome do arquivo de entrada e gera o arquivo de saida. Executa insercao, delecao e imprime aqui*/
void executaB(char* nomeArqEntrada, char *nomeArqSaida){
    FILE *arqSaida;
    arqSaida = fopen(nomeArqSaida,"w"); //crio arquivo final de saida

    Btree *a = Inicializa();
    char comando;
    int chave;

    FILE *arq;
    arq = fopen(nomeArqEntrada,"r"); //leio o arquivo de entrada

    if(arq == NULL)
			printf("Erro, nao foi possivel abrir o arquivo\n");

    else{
        while((fscanf(arq,"%c %i\n", &comando, &chave))!=EOF){
            if(comando == 'p'){
                //se pagina vazia, imprime 'Vazia' na saida.txt
                if(a == NULL){
                    fprintf(arqSaida,"Vazia\n");
                }
                else{
                    //imprime
                    imprimePreOrdem(a,nomeArqSaida); //passo o nome do arquivo de saida para guardar a resposta
                }
            }
            else if(comando == 'i'){
                //primeira vez que vai inserir na pagina: cria pagina e insere
                if(a == NULL){
                    a = CriaBtree();
                    a = insere(a,chave);
                }
                else{
                      a = insere(a,chave);
                }
            }
            else if(comando == 'r'){
                pesquisaRemove(a,chave);
                //fprintf(arqSaida,"\n");
            }
        }
    }
    fclose(arqSaida);
    fclose(arq);
}

int main (int argc, char *argv[]) {

    executaB(argv[1],argv[2]);

    return 0;
}

