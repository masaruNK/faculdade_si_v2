/*Exercicio Programa 1 - AED 1 Prof. Marcelo Morandini

Alunos: Douglas Andrade N�USP: 8061692
        Kaio Nakazono   N�USP: 5617059*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct estrutura{
    int chave;
    char nome[100];
    struct estrutura* prox;
}NO;

typedef struct{
    NO* cabeca;
}LISTA;

void inicializar(LISTA *l){
    l->cabeca = (NO*)malloc(sizeof(NO));
    l->cabeca->prox = l->cabeca;
}

void imprime(LISTA* l){
    printf("Impressao da lista: ");
    NO* p = l->cabeca->prox;
    if(p == l->cabeca){
        printf("***Lista vazia!***\n");
    }else{
         while(p != l->cabeca){
        printf("%s ", p->nome);
        p = p->prox;
        }
    }
    printf("\n");
}

NO* primeiroElemLista(LISTA* l){
    if(l->cabeca->prox == l->cabeca) return NULL;
    else return l->cabeca->prox;
}

NO* ultimoElemLista(LISTA* l){
    NO* p = l->cabeca->prox;
    if(p == l->cabeca)return NULL;
    while(p->prox != l->cabeca){
        p = p->prox;
    }
    return p;
}

NO* enesimoElemLista(LISTA *l, int n){
    NO* p = l->cabeca->prox;
    int i = 1;
    while((p->prox != l->cabeca) && (i < n)){
        p = p->prox;
        i++;
    }
    if(i != n)return NULL;
    else return (p);
}

int tamanhoLista(LISTA* l){
    NO* p = l->cabeca->prox;
    int tam = 1;
    if(p == l->cabeca) return tam-1;
    while(p->prox != l->cabeca){
        tam++;
        p = p->prox;
    }
    return tam;
}


void inserirElem(LISTA* l){
    char name[100];
    NO* novo;
    NO* p = l->cabeca->prox;
    novo = (NO*)malloc(sizeof(NO));
    printf("Digite o nome da crianca: ");
    scanf("%100s", name);
    strcpy(novo->nome, name);
    while(p->prox != l->cabeca){
        p = p->prox;
    }
    p->prox = novo;
    novo->prox = l->cabeca;
}

NO* busca(LISTA* l, int pos, NO* *ant){
    NO* p = l->cabeca->prox;
    *ant = NULL;
    int i = 1;
    while((p->prox != l->cabeca) && (i < pos)){
        *ant = p;
        p = p->prox;
        i++;
    }

    if(i == pos)return p;
    return NULL;
}

void excluirElem(LISTA* l){
    NO* p;
    NO* ant;
    int pos;
    /*Obtem a quantidade de elem na lista*/
    int tam = tamanhoLista(l);
    if(tam == 0){
        printf("***Lista vazia!***");
    }
    else if(tam == 1){
        printf("***A lista so tem uma crianca: %s, que sera considerada vencedora***\n",l->cabeca->prox->nome);
    }
    else{
        //printf("Qtd. de crian�as antes: %d\n",tam); //debug
        srand(time(0)); //obtem a semente
        /*Obtem a pos da crianca que sera excluida utilizando a funcao rand().
        Nesse caso, rand() % pos, ira retornar um numero entre 0 e pos - 1 da lista.
        Para evitar o zero (a primeira crianca da lista tem posicao 1) somamos + 1, alem
        de incluir o pos. Ex: temos 4 elem na lista --> se rand() % 2 ira retorna um numero
        entre 0 e 3. Se (rand() % 2) + 1 ira retorna um numero entre 1 e 4.*/
        pos = (rand() % tam) + 1;
        //printf("Pos: %d\n",pos);//debug
        p = busca(l, pos, &ant);
        if(p == NULL)return; // nada a excluir
        if(ant == NULL)l->cabeca->prox = p->prox; // exclui o 1� elemento
        else ant->prox = p->prox; // exclui elemento que possui ant

        //tam = tamanhoLista(l);//debug
        //printf("Qtd. de criancas depois: %d\n",tam);//debug
        printf("***Crianca %s foi excluida!***\n",p->nome);
        free(p);

        if(tam == 1){
            printf("\n!!! A crianca %s foi a vencedora :) !!!\n",l->cabeca->prox->nome);
        }
    }
}


void help(){
    printf("*Comandos validos*:\n");
    printf("1 - Inserir elementos\n");
    printf("2 - Excluir\n");
    printf("3 - Imprimir\n");
    printf("4 - Mostrar comandos\n");
    printf("5 - Sair\n");
}

int main(){
    LISTA l;
    inicializar(&l);

    help();
    char comando = ' ';
    scanf("%c",&comando);
    while(comando != '5'){
        switch(comando){
            case '1' :  inserirElem(&l);
                        printf("Valor inserido!\n> Digite um novo comando < : ");
                        break;
            case '2' :  excluirElem(&l);
                        printf("\n> Digite um novo comando < : ");
                        break;
            case '3' :  imprime(&l);
                        printf("> Digite um novo comando <: ");
                        break;
            case '4' :  help(); break;
            default  :  {while(comando != '\n')
                            scanf("%c",&comando);};

        }
        scanf("%c", &comando);
    }
    printf("Saiu do programa\n");

    return 0;
}

