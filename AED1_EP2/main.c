/*  EP 2 - Disciplina: AED I    Prof. Marcelo Morandini   2� semestre de 2017
    Alunos: Douglas Andrade N�USP: 8061692
            Kaio Nakazono   N�USP: 5617059

*Fontes consultadas:
-https://www.youtube.com/watch?v=fUxnb5eTRS0

-https://www.youtube.com/watch?v=rA0x7b4YiMI

-http://www.vision.ime.usp.br/~pmiranda/mac122_2s14/aulas/aula13/aula13.html

*OBS:
-Para prefixa, utilizamos 3 pihas. Uma pilha para conter a expressao de forma invertida,
a segunda como pilha auxiliar e a terceira, como expressao na forma prefixa.
-Para posfixa, utilizamos uma pilha com recurso auxiliar e uma fila como expressao na forma
posfixa.

*/

#include <stdio.h>
#include <stdlib.h>

typedef struct estrutura{
    char chave;
    struct estrutura *prox;
}NO;

typedef struct{
    NO* topo;
}PILHA;

typedef struct{
    NO* inicio;
    NO* fim;
}FILA;

void inicializarPilha(PILHA *p){
    p->topo = NULL;
}

void inicializarFila(FILA *f){
    f->inicio = NULL;
    f->fim = NULL;
}

void inserirFila(char ch, FILA* f){
    NO* novo = (NO*)malloc(sizeof(NO));
    novo->chave = ch;
    novo->prox = NULL;
    if(f->fim) f->fim->prox = novo;
    else f->inicio = novo;
    f->fim = novo;
}

void push(char ch, PILHA *p){
    NO* novo = (NO*)malloc(sizeof(NO));
    novo->chave = ch;
    novo->prox = p->topo;
    p->topo = novo;
}

char pop(PILHA *p){
    char ch;
    NO* aux;
    if(p->topo == NULL)return NULL;
    aux = p->topo;
    ch = aux->chave;
    p->topo = p->topo->prox;
    free(aux);
    return ch;
}

void liberaPilha(PILHA *p){
    NO* p1 = p->topo;
    NO* aux;
    while(p1){
        aux = p1->prox;
        free(p1);
        p1 = aux;
    }
}

//Imprime pilha. Usada para imprimir a expressao prefixa.
void imprime(PILHA *p){
    NO* aux = p->topo;
    printf("*Prefix: ");
    while(aux){
        printf("%c", aux->chave);
        aux = aux->prox;
    }
    printf("\n");
}

//Imprime fila. Usada para imprimir a expressao posfixa.
void imprimeFila(FILA* f){
    NO* aux = f->inicio;
    printf("*Posfix: ");
    while(aux != NULL){
        printf("%c",aux->chave);
        aux = aux->prox;
    }
    printf("\n");
}

/*Funcao que retorna um inteiro positivo ou negativo, indicando
quem tem maior prioridade. Se retornar um numero positivo, entao
'c' (elemento char que esta sendo lida da expressao infixa) tem maior
prioridade que 't' (primeiro elemento char (topo) da pilha auxiliar)*/

int prioridade2(char c, char t){
    int prc, prt;

    if(c == '^')
        prc = 4;
    else if(c == '*' || c == '/')
        prc = 2;
    else if(c == '+' || c == '-')
        prc = 1;
    else if(c == '(' || c == ')')
        prc = 0;

    if(t == '^')
        prt = 4;
    else if(t == '*' || t == '/')
        prt = 2;
    else if(t == '+' || t == '-')
        prt = 1;
    else if(t == '(' || c == ')')
        prt = 0;

    return (prc - prt);
}

/*Funcao que transfoma uma expressao infixa em prefixa.*/
void infixToPrefix(char expr[]){
    PILHA pInv; //guarda a expressao original (infixa) de forma invertida
    PILHA pAux; //pilha aux. que guarda os operadores
    PILHA prefix; //resultado final, ou seja, a expr. prefixa
    int tam = strlen(expr); //qtd. de elementos da expressao infixa
    int  i = 0;
    char c, t; //char 'c' le a exp. infixa e 't' le a pilha aux.
    inicializarPilha(&pInv);
    inicializarPilha(&pAux);
    inicializarPilha(&prefix);
    //colocando na pilhaInv. Inverte a expressao infixa.
    while(1){
        c = expr[i];
        push(c,&pInv);
        i++;
        if(i > tam-1)
            break;
    }

    /*Prefix*/
    NO* auxInv = pInv.topo;
    while(auxInv){
        c = auxInv->chave;
        auxInv = auxInv->prox;
        if(c == ')'){
            push(c,&pAux);
        }
        else if(c >= 'a' && c <= 'z'){
            push(c,&prefix);
        }
        else if(c == '+' || c == '-' || c == '*' || c == '/' || c == '^'){
            t = pop(&pAux); //para iniciar a comparacao entre 'c' e 't'
            if(t){
                if(prioridade2(c,t) >= 0){ //se t tem menor prioridade, tanto 'c' quanto 't' ficam na pilha aux.
                    push(t,&pAux);
                    push(c,&pAux);
                }else if(prioridade2(c,t) < 0){ //se t tem maior prioridade
                    while(t && prioridade2(c,t) < 0){
                        push(t,&prefix); //guardo 't' na pilha prefix
                        t = pop(&pAux);
                        if(t == ')') break; //achou um ')' para, pois ja lemos todos os valores dentro do '()' original
                    }
                    if(t != ')') push(t,&pAux); //caso operadores seja iguais (saiu causa da cond. do while), ver exemplo com: a+b*c+d
                    // c tem menor prioridade que t, push no pAux
                    push(c,&pAux);

                }
            }
            //Pilha vazia, push direto sem comparacao com o item que j� est� na pilha
            else{
                push(c,&pAux);
            }
        }
        //trigger, insere todos os elem da pilha para o prefix ate achar ')'
        else if(c == '('){
            t = pop(&pAux);
            while(t && t != ')'){
                push(t,&prefix);
                t = pop(&pAux);
            }
        }
    }//fim while

    //pop o resto dos elementos da pilha aux.
    t = pop(&pAux);
    while(t){
        push(t,&prefix);
        t = pop(&pAux);
    }
    liberaPilha(&pAux);
    liberaPilha(&pInv);

    imprime(&prefix);

}

/*Posfix*/
void infixToPosfix(char expr[]){
    PILHA pAux;	//Pilha aux. que ira guarda os operadores
    FILA posfixa; //Fila que ira guardar a expr. posfixa
    char c, t;
    int i = 0;
    int tam = strlen(expr);
    inicializarPilha(&pAux);
    inicializarFila(&posfixa);

   //Le toda a expressao infixa
    while(i < tam){
        c = expr[i];
        i++;
        if(c >= 'a' && c <= 'z'){
            inserirFila(c,&posfixa);
        }
        else if(c == '('){
            push(c,&pAux);
        }
        else if(c == '+' || c == '-' || c == '*' || c == '/' || c == '^'){
            t = pop(&pAux); //Para comparar com 'c'
            if(t){
                if(prioridade2(c,t) > 0){
                    push(t,&pAux);
                    push(c,&pAux);
                }
                else if(prioridade2(c,t) <= 0){ //t tem maior prioridade
                    while(t && prioridade2(c,t) <= 0){
                        inserirFila(t,&posfixa);


                        t = pop(&pAux);
                        if(t == ')') break;
                    }
                    if(t && t != ')') push(t,&pAux);
                    push(c,&pAux);
                }

            }
            //Pilha aux. vazia, push 'c' direto
            else{
                push(c,&pAux);
            }
        }
        else if(c == ')'){
            t = pop(&pAux);
            while(t && t != '('){
                inserirFila(t,&posfixa);
                t = pop(&pAux);
            }
        }
    }//fim while

    //Pop os elementos restantes da pilha aux.
    t = pop(&pAux);
    while(t){
        inserirFila(t,&posfixa);
        t = pop(&pAux);
    }
    liberaPilha(&pAux);

    imprimeFila(&posfixa);
}


int main(){
    char expr[] = "a+b*c+d";
    infixToPrefix(expr);
    infixToPosfix(expr);
    //system("pause");
    return 0;
}

