﻿O objetivo deste exercício-programa é avaliar o desempenho de dois
protocolos de roteamento diferentes utilizados em redes sem fio:
Optimized Link State Routing (OLSR) e o Ad hoc On-Demand Distance
Vector Routing (AODV). Para isso, utilize o simulador de redes
NS-3 [https://www.nsnam.org/]

Vocês (grupos de 2 a 4 pessoas) devem criar um conjunto de
experimentos para avaliar as diferenças entre os dois protocolos de
roteamento e escrever um relatório detalhado com informações sobre
como os experimentos foram realizados e comparando os seus
resultados. Seu relatório deve conter os scripts desenvolvidos, uma
explicação detalhada de como os scripts funcionam, além de uma
descrição detalhada de como os dois protocolos funcionam e suas
principais diferenças.

Seus casos de teste devem contemplar cenários com redes criadas
aleatoriamente e com redes criadas de forma fixa. Para cada tipo de
cenário, teste o desempenho em redes com número de nós diferentes (e
com ordens de magnitudes diferentes). Para os cenários aleatórios,
repita os experimentos diversas vezes (n ≥ 20) e apresente as médias
dos resultados.

Faz parte do exercício o estabelecimento de métricas comuns de
desempenho adequadas a ambos os protocolos (ex: número de mensagens
trocadas, tempo para determinar as melhores rotas, etc.)


----------------------------------------------------------------------

