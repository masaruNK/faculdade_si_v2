#Nome: Arthur Kubo  NºUSP:

from copy import deepcopy

def LeiaMatriz(Mat, NomeArq):
    #Retorna true se conseguiu ler o arquivo e false caso contrario

    #Abrir o arquivo para leitura
    try:
        arq = open(NomeArq, "r")
    except:
        print("erro no open")
        return False
    
    #Ler cada uma das linhas do arquivo
    i = 0
    for linha in arq:
        #Se der alguma exception retorna false
        try:
            v = linha.split() #Separa os elementos da string
            Mat.append([]) #Adiciona uma nova linha a matriz
            #Transforma os strings numericos em numeros inteiros e adiciona #cada int à matriz
            for j in range(len(v)):
                Mat[i].append(int(v[j]))
            i = i + 1
        except:
            #Algum erro no trecho acima
            print("erro no split, int ou nos appends")
            return False
        
    #Consistencia dos valores da matriz:

    #Verificar se todas as linhas tem o mesmo numero de elementos
    totalColunas = len(Mat[0]) #a qtd de colunas da 1ª linha deve ser igual a das outras linhas
    contLinha = 0 #contador
    while(contLinha < i): #para cada linha verifico a qtd de colunas
        colunas = len(Mat[contLinha])
        if(colunas != totalColunas):
            print("erro, numero de colunas diferentes")
            print("linha com qtds != de colunas: {}".format(contLinha))
            return False
        contLinha = contLinha + 1
    
    #Verificar se todos os elementos sao 0s e -1s
    contLinha = 0
    while(contLinha < i):
        #print("Linha: {}".format(contLinha)) #debug
        for k in Mat[contLinha]:
            #print(k, end = ' ') #debug
            if(Mat[contLinha][k] != 0 and Mat[contLinha][k] != -1):
                print("Erro: valor diferente na posicao {}, {}".format(contLinha, k))
                return False
        contLinha = contLinha + 1

    arq.close()
    return True
#Fim LeiaMatriz

def numera_vizinhos(mat, i, j, k):

    totalLinhas = len(mat)
    #print("total linhas: {}".format(totalLinhas))
    totalColunas = len(mat[0])
    #print("total colunas: {}".format(totalColunas))
    totalElementos = totalColunas * totalLinhas
    cont = 0

    mat[i][j] = k

    while(cont < totalElementos):
        for m in range(len(mat)):
            #print("m: {}".format(m), end=" ") #debug
            for n in range(len(mat[m])):
                #print("n: {}".format(n), end=" ") #debug
                #Se achou elemento, numero seus vizinhos (se possivel)
                if(mat[m][n] == k):
                    #print("entrou no primeiro if")
                    if(cima(totalLinhas, m - 1)):
                        if(mat[m-1][n] == 0):
                            mat[m-1][n] = k + 1
                    if(direita(totalColunas, n + 1)):
                        if(mat[m][n+1] == 0):
                            mat[m][n+1] = k + 1
                    if(baixo(totalLinhas, m + 1)):
                        if(mat[m+1][n] == 0):
                            mat[m+1][n] = k + 1
                    if(esquerda(totalColunas, n - 1)):
                        if(mat[m][n-1] == 0):
                            mat[m][n-1] = k + 1
        k = k + 1     
        cont = cont + 1
#Fim numera_vizinhos

#Funcoes auxiliares para a numeracao dos vizinhos, determinando se determinado elemento
#[i,j] possui vizinhos (cima, direita, baixo, esquerda)
def cima(totalLinhas, i):
    if (i >= 0):
        return True
    return False

def baixo(totalLinhas, i):
    if(i < totalLinhas):
        return True
    return False

def esquerda(totalColunas, j):
    if(j >= 0):
        return True
    return False

def direita(totalColunas, j):
    if(j < totalColunas):
        return True
    return False

#Funcao que chama numera_vizinhos() para cada porta encontrada.
#Também chama as funcoes imprimeMatriz() e imprime_mais_distante().
def executa(mat):
    ultimaLinha = len(mat) - 1
    ultimaColuna = len(mat[0]) - 1

    matCopy = deepcopy(mat)

    imprimeMatriz(mat, 0) #Imprime a matriz original (0's e -1's)
    print()
    #Borda superior (analisa os elementos da coluna da primeira linha em busca de uma porta):
    '''
    x x x
    - - -
    - - -
    '''
    for j in range(len(matCopy[0])):
        if(matCopy[0][j] == 0):
            print("Porta [{}, {}]:".format(0,j))
            numera_vizinhos(matCopy, 0, j, 1)
            imprimeMatriz(matCopy, 1)
            print()
            imprime_mais_distante(matCopy, 0, 0, 1)
            matCopy = deepcopy(mat)
    
    #Borda esquerda:
    '''
    - - -
    x - -
    - - -
    '''
    for i in range(len(matCopy)):
        if(i != 0 and i != len(matCopy)-1): #Para nao repetir o primeiro e ultimo elemento ja visto na borda superior e inferior
            if(matCopy[i][0] == 0):
                print("Porta [{}, {}]:".format(i,0))
                numera_vizinhos(matCopy, i, 0, 1)
                imprimeMatriz(matCopy, 1)
                print()
                imprime_mais_distante(matCopy, 0, 0, 1)
                matCopy = deepcopy(mat)
    
    #Borda inferior:
    '''
    - - - 
    - - -
    x x x
    '''
    for j in range(len(matCopy[0])):
        if(matCopy[ultimaLinha][j] == 0):
            print("Porta [{}, {}]:".format(ultimaLinha, j))
            numera_vizinhos(matCopy, ultimaLinha, j, 1)
            imprimeMatriz(matCopy, 1)
            print()
            imprime_mais_distante(matCopy, 0, 0, 1)
            matCopy = deepcopy(mat)

    #Borda direita:
    '''
    - - -
    - - x
    - - -
    '''
    for i in range(len(matCopy)):
        if(i != 0 and i != len(matCopy)-1): #Para nao repetir o primeiro e ultimo elemento ja visto na borda superior e inferior
            if(matCopy[i][ultimaColuna] == 0):
                print("Porta [{}, {}]:".format(i, ultimaColuna))
                numera_vizinhos(matCopy, i, ultimaColuna, 1)
                imprimeMatriz(matCopy, 1)
                print()
                imprime_mais_distante(matCopy, 0, 0, 1)
                matCopy = deepcopy(mat)
#Fim executa

#Imprime a matriz original
def imprimeMatriz(mat, flag):
    linhas = len(mat)
    colunas = len(mat[0])
    if(flag == 0):
        print("Matriz labirinto com {} linhas por {} colunas".format(linhas, colunas))
    elif(flag == 1):
        print("Caminhos possiveis e comprimento: ")
    for i in range(len(mat)):
        for j in range(len(mat[i])):
            print(mat[i][j], end = "\t")
        print()
#Fim imprimeMatriz

#Imprime a maior distancia e sem caminho
def imprime_mais_distante(mat, i, j, dist):
   #Nao usei os parametros i, j e dist, porque faco uma
   #varredura completa na matriz.
   
    maiorDist = maiorValor(mat, dist)

    #Elementos com maior distancia
    print("Posicoes com caminho de maximo comprimento: ")
    for k in range(len(mat)):
        for m in range(len(mat[k])):
            if(mat[k][m] == maiorDist):
                print("[{}, {}]".format(k,m))
    
    print()

    #Elementos com distancia nula
    print("Posicoes sem caminho: ")
    for k in range(len(mat)):
        for m in range(len(mat[k])):
            if(mat[k][m] == 0):
                print("[{}, {}]".format(k,m))
    
    print()
#Fim imprime_mais_distante            
         
#Funcao auxiliar que retorna o maior valor na matriz. Utiliza busca sequencial        
def maiorValor(mat, dist):
    maximo = dist
    for i in range(len(mat)):
        for j in range(len(mat[i])):
            if(mat[i][j] > maximo):
                maximo = mat[i][j]
    return maximo
#Fim maiorValor

def main():
    nomeArquivo = input("Entre com o nome do arquivo: ")
    mat = []
    if(LeiaMatriz(mat, nomeArquivo)): #retorna true ou false
        executa(mat)

if __name__ == "__main__":
    main()



