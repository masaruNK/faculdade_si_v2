/*********************************************************************/
/**   Aluno: Kaio Masaru Nakazono                  num USP: 5617059 **/
/**                                                                 **/
/**   data entrega: 02/05/2015                                      **/
/*********************************************************************/

/* Programa que calcula os juros mensais necessario para 
 * atingir um determinado saldo, sendo realizado uma serie
 * de depositos mensais*/

class NewtonRaphson {
	/*
		Depositos realizados, com saldo final
	*/
	static double[] depositos = new double[11];
	
	/*
		Datas correspondentes aos depositos feitos e saldo final
	*/
	static int[] datas = new int[11];

	/*
		Metodo para calculo dos juros de aplicacao, segundo Newton-Raphson
	*/
	
	static double newton(double epsilon) {
		
		//calculando f(j)
		if(epsilon <= 0 || epsilon >= 1) return -1; //se epsilon <= 0 ou >= 0, sai da aplicacao e retorna -1 (erro)
		else{
			double j = 0.5; //palpite inicial
			double jk = 0; //juros no passo seguinte
			double x = 1; // |jk - j|
			double f = 0; double f2 = 0; //f = antes, f2 = posterior
			while (x >= epsilon) {  //quando x for menor que epsilon, sai do laco e retorna o juros
				f = depositos[0]*Math.pow(1+j, datas[10]-datas[0]) + depositos[1]*Math.pow(1+j, datas[10]-datas[1]) + depositos[2]*Math.pow(1+j, datas[10]-datas[2])
						+ depositos[3]*Math.pow(1+j, datas[10]-datas[3]) + depositos[4]*Math.pow(1+j, datas[10]-datas[4]) + depositos[5]*Math.pow(1+j, datas[10]-datas[5])
						+ depositos[6]*Math.pow(1+j, datas[10]-datas[6]) + depositos[7]*Math.pow(1+j, datas[10]-datas[7]) + depositos[8]*Math.pow(1+j, datas[10]-datas[8])
						+ depositos[9]*Math.pow(1+j, datas[10]-datas[9]) - depositos[10];
				
				f2 = (datas[10] - datas[0])*depositos[0]*Math.pow(1+j, datas[10]-datas[0]-1) + (datas[10] - datas[1])*depositos[1]*Math.pow(1+j, datas[10]-datas[1]-1) +
						(datas[10] - datas[2])*depositos[2]*Math.pow(1+j, datas[10]-datas[2]-1) + (datas[10] - datas[3])*depositos[3]*Math.pow(1+j, datas[10]-datas[3]-1) +
						(datas[10] - datas[4])*depositos[4]*Math.pow(1+j, datas[10]-datas[4]-1) + (datas[10] - datas[5])*depositos[5]*Math.pow(1+j, datas[10]-datas[5]-1) +
						(datas[10] - datas[6])*depositos[6]*Math.pow(1+j, datas[10]-datas[6]-1) + (datas[10] - datas[7])*depositos[7]*Math.pow(1+j, datas[10]-datas[7]-1) +
						(datas[10] - datas[8])*depositos[8]*Math.pow(1+j, datas[10]-datas[8]-1) + (datas[10] - datas[9])*depositos[9]*Math.pow(1+j, datas[10]-datas[9]-1);
				
				
				jk = j - (f/f2);
				x = Math.abs(j - jk);
				//System.out.print("jk = "+jk+"\tx = "+x+"\n");
				j = jk;
			}
			return j; //retorna o juros
		}
	}
	
	/*
		Use isso apenas para seus testes. Ele pode ate ser removido para entrega. Use-o para abastecer valores nos arranjos e entao testar o 
		metodo newton.
		
		O MAIN SERAO COMPLETAMENTE IGNORADO.
	*/
	public static void main(String[] args) {
				
				/*depositos[0] = 0;
				depositos[1] = 0;
				depositos[2] = 0;
				depositos[3] = 1000;
				depositos[4] = 1200;
				depositos[5] = 100;
				depositos[6] = 0;
				depositos[7] = 1100;
				depositos[8] = 0;
				depositos[9] = 900;
				depositos[10] = 5000;
				
				datas[0] = 0;
				datas[1] = 0;
				datas[2] = 0;
				datas[3] = 3;
				datas[4] = 4;
				datas[5] = 5;
				datas[6] = 0;
				datas[7] = 7;
				datas[8] = 0;
				datas[9] = 9;
				datas[10] = 12;
				
				double resp = newton(0.001);
				System.out.println(resp);*/
		
	}
}
