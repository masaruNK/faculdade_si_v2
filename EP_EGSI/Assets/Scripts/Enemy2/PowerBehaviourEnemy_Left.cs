using UnityEngine;
using System.Collections;

public class PowerBehaviourEnemy_Left : MonoBehaviour {
	
	float speed = 8f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		transform.Translate(Vector3.left*speed*Time.deltaTime);

	}

}
