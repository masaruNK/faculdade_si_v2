﻿using UnityEngine;
using System.Collections;

public class DestroyPowerEnemy2 : MonoBehaviour {

	//public ParticleSystem particula;

	/*void OnCollisionEnter(Collision hit){
		if(hit.gameObject.tag == "Player"){
			Destroy(gameObject);
		}
	}*/

	//caso o poder no inimigo 2 bata no inimigo 1. Apenas destruimos esse poder e ira aparecer algumas
	//particulas
	void OnTriggerEnter(Collider hit){
		if(hit.gameObject.tag == "Enemy"){ //identifica o objeto que entramos no trigger
			//Nao colocar o inimigo Black com a tag Enemy, senao nao soltara o power
			Debug.Log("Inimigo");
			//Instantiate(particula, hit.transform.position, Quaternion.identity);
			Destroy(gameObject);
			//Destroy(particula);
		}
		if(hit.gameObject.tag == "Wall"){
			Destroy(gameObject);
		}
		if(hit.gameObject.tag == "Player"){
			Destroy(gameObject);
		}
	}
	
	void OnBecameInvisible(){
		Destroy(gameObject);
	}
}
