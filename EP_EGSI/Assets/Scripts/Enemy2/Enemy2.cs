/*Script que faz o enemy 2 atacar o poder pelos quatro lados do cubo.*/

using UnityEngine;
using System.Collections;

public class Enemy2 : MonoBehaviour {

	//power
	public Transform power1;
	public Transform power2;
	public Transform power3;
	public Transform power4;
	public float time = 5f;
	float currentTime = 0.0f;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		Attack();
	}

	void Attack(){
		currentTime += Time.deltaTime;
		Debug.Log(currentTime);
		if(currentTime >= time){
			Instantiate(power1, transform.position, transform.rotation);
			Instantiate(power2, transform.position, transform.rotation);
			Instantiate(power3, transform.position, transform.rotation);
			Instantiate(power4, transform.position, transform.rotation);
			currentTime = 0.0f;
		}

	}

}
