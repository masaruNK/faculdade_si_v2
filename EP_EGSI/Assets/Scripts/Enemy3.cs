using UnityEngine;
using System.Collections;

public class Enemy3 : MonoBehaviour {

	public Transform[] power;
	public float time = 5f;
	float currentTime = 0.0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		Attack ();
	}

	void Attack(){
		currentTime += Time.deltaTime;
		//Debug.Log(currentTime);
		if(currentTime >= time){
			for(int i = 0; i < power.Length; i++){
				Instantiate(power[i], transform.position, Quaternion.identity);
			}
			currentTime = 0.0f;
		}
	}
}
