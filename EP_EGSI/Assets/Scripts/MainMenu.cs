﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public GUISkin skin;

	void OnGUI(){

		GUI.skin = skin;

		GUI.Label(new Rect(10,10,400,80),"Cubo");
		if(GUI.Button(new Rect(10,100,100,45),"Play")){

			Application.LoadLevel(1);
		}
		if(GUI.Button(new Rect(10,160,100,45),"Quit")){

			Application.Quit();
		}
	}
}
