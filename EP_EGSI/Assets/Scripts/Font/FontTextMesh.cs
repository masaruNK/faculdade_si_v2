﻿using UnityEngine;
using System.Collections;

public class FontTextMesh : MonoBehaviour {

	public Font newFont;

	// Use this for initialization
	void Start () {
		GetComponent<TextMesh>().font = newFont;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
}
