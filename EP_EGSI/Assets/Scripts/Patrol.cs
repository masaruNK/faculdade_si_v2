using UnityEngine;
using System.Collections;

public class Patrol : MonoBehaviour {

	public Transform[] patrolPoints;
	private int currentPoint;
	public float moveSpeed;

	public ParticleSystem particula;

	// Use this for initialization
	void Start () {
	
		currentPoint = 0;
		transform.position = patrolPoints[currentPoint].position;
	}
	
	// Update is called once per frame
	void Update () {

		Perseguicao();


	}

	void Perseguicao(){

		if(transform.position == patrolPoints[currentPoint].position){
			currentPoint++;
		}
		if(currentPoint >= patrolPoints.Length){
			currentPoint = 0;
		}
		transform.position = Vector3.MoveTowards(transform.position,
		    patrolPoints[currentPoint].position,moveSpeed*Time.deltaTime);
	}

	/*void OnTriggerEnter(Collider hit){
		if(hit.gameObject.tag == "Enemy2_power"){ //identifica o objeto que entramos no trigger
			//Debug.Log("Inimigo");
			Instantiate(particula, hit.transform.position, Quaternion.identity); //quando o Enemy eh atingido solta a particula 
			//Destroy(gameObject);
			//Destroy(particula);
		}
	}*/
	

}
