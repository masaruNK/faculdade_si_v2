﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	//Count
	public int currentScore;
	public int highScore;
	private int currentLevel = 2; 
	public int unlockLevel;
	private int levels = 4; //qtd de levels, fases
	public int tokenCount = 0;
	private int totalTokenCount;

	//GUI skin
	public GUISkin skin;
	
	//Timer variables
	private float startTime = 20f;
	private string currentTime;
	public Rect timerRect;
	public Color warningColorTime;
	public Color defaultColorTime;
	private float alertTime = 5f; //5 segundos para o fim, o GUI time ficara vermelho (warningTime)
	private bool catchToken = false; //quando pega o token o tempo volta aos 20 segundos

	//References
	//public GameObject tokenParent;

	//Outras variaveis
	private bool showWinScreen; //mostra a tela 
	public int winScreenWidth, winScreenHeight;



	void Start(){
		//totalTokenCount = tokenParent.transform.childCount; //calcula quantos filhos tokenParent possui
		/*if(PlayerPrefs.GetInt("Level Completed") > 0){
			PlayerPrefs.GetInt("Level Completed");
		}else{
			currentLevel = 2; //fase atual por causa da ordem, ver em Build Settings
		}*/

		//DontDestroyOnLoad(gameObject); //quando carrega outra cena, o GameManager nao eh destruido
	}

	void Update(){
		//coinGame = coin.Coins();
		//GameTime();
	}

	public void CompleteLevel(){
		showWinScreen = true;

	}

	void LoadNextLevel(){
		if(currentLevel < levels){ //enquanto nao estiver na ultima fase, ira incrementar o currentLevel
			currentLevel++;
			print (currentLevel);
			SaveGame();
			Application.LoadLevel(currentLevel); 
		}else
			print("You win!");
	}

	void SaveGame(){
		PlayerPrefs.SetInt("Level Completed",currentLevel);
		PlayerPrefs.SetInt("Level " + currentLevel.ToString()+" score",currentLevel);
		PlayerPrefs.GetInt("Level Completed");
	}

	//todo GUI deve estar no metodo OnGUI ? -> pesquisar
	void OnGUI(){
		GUI.skin = skin;
		if(showWinScreen){
			Rect winScreenRect = new Rect(Screen.width/2 - (Screen.width*.4f/2), Screen.height/2 - (Screen.height*.4f/2),Screen.width*.4f,Screen.height*.4f);
			GUI.Box(winScreenRect,"Ola Mundo!");

			if(GUI.Button(new Rect(winScreenRect.x + winScreenRect.width -170 , winScreenRect.y + winScreenRect.height -60, 150, 40), "Continue")){
				LoadNextLevel();
			}
			
			if(GUI.Button(new Rect(winScreenRect.x + 20 , winScreenRect.y + winScreenRect.height -60, 150, 40), "Quit")){
				Application.LoadLevel("menu");
			}

			GUI.Label(new Rect(winScreenRect.x + 20, winScreenRect.y + 40, 300, 50), "Ola");
		}
	}

	//pega o token
	/*public void AddToken(){
		tokenCount++;
		catchToken = true;
	}*/

	//mostra o tempo
	/*void OnGUI(){
		GUI.skin =skin;
		if(startTime < alertTime){
			skin.GetStyle("Timer").normal.textColor = warningColorTime;
		}else{
			skin.GetStyle("Timer").normal.textColor = defaultColorTime;
		}
		GUI.Label(timerRect, currentTime, skin.GetStyle("Timer"));
		GUI.Label(new Rect(1100,100,200,200), tokenCount.ToString()+"/"+ totalTokenCount.ToString());

		if(showWinScreen){
			Rect winScreenRect = new Rect(Screen.width/2-(winScreenWidth/2),Screen.height/2-(winScreenHeight/2),winScreenWidth,winScreenHeight); 
			//Screen.width e height: tamanho da jenla do jogo, nao do monitor
			GUI.Box(winScreenRect,"Vitoria!");
		}
	}

	public void GameTime(){
		startTime -= Time.deltaTime;
		currentTime = string.Format("{0:0.0}",startTime);
		if(catchToken){ 
			startTime = 20; //se pegou o token, volta o tempo aos 20s
		}
		
		if(startTime <= 0){
			startTime = 0;
			Destroy(gameObject); //se tirar essa linha: quando voltar ao menu (ao acabar o tempo),
			//ira aparecer o tempo na tela de menu e tmb o botao Play nao ira funcionar
			Application.LoadLevel(0);
		}
		catchToken = false;
	}*/
	

}
