﻿using UnityEngine;
using System.Collections;


/*
 ESTE SCRIPT NAO EH MAIS UTILIZADO!!!
 */
public class Tempo : MonoBehaviour {

	//public int totalCoins;
	public TextMesh startTime;
	private float currentTime;
	public Color warningColorTime;
	public Color defaultColorTime;
	//private float alertTime = 5f; //5 segundos para o fim, o GUI time ficara vermelho (warningTime)
	//private bool catchToken = false; //quando pega o token o tempo volta aos 20 segundos

	// Use this for initialization
	void Start () {
		currentTime = 0f;
		//startTime.text = "20";
		startTime.text = string.Format("{0:0.0}", currentTime);
	

	}
	
	// Update is called once per frame
	void Update () {

		//totalCoinsLevel.text = totalCoins.ToString();
		 GameTime();
	}

	/*public void GameTime(){
		startTime.color = Color.white;
		currentTime -= Time.deltaTime;
		startTime.text = string.Format("{0:0.0}", currentTime);
		if(catchToken){ 
			currentTime = 20f;
			startTime.text = string.Format("{0:0.0}", currentTime); //se pegou o token, volta
			//o tempo aos 20s
		}
		
		if(currentTime <= 0){
			startTime.text = string.Format("{0:0.0}", currentTime);
			Destroy(gameObject); //se tirar essa linha: quando voltar ao menu (ao acabar o tempo),
			//ira aparecer o tempo na tela de menu e tmb o botao Play nao ira funcionar
			Application.LoadLevel(0);
		}
		if(currentTime <= 5){
			startTime.color = Color.red;
		}
		catchToken = false;
	}*/

	public void GameTime(){

		startTime.color = Color.white;
		currentTime += Time.deltaTime;
		startTime.text = string.Format("{0:0.0}", currentTime);

	}

	//pega o token
	/*public void AddToken(){
		catchToken = true;
	}*/
}
