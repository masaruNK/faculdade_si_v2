﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//continue
	public void GoToSelectLevel(){
		Application.LoadLevel("Select Level");
	}

	//sai do jogo
	public void Exit(){
		Application.Quit();
	}

	//inicia na primeira fase
	public void PlayGame(){
		Application.LoadLevel("Level1");
	}
}
