﻿/*manda a mensagem (metodo) do botao para a aplicacao*/

using UnityEngine;
using System.Collections;

public class ButtonMessageBehaviour : MonoBehaviour {

	public string nameMessageMethod;
	public GameObject target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick(){
		target.SendMessage(nameMessageMethod, SendMessageOptions.DontRequireReceiver);
	}
}
