﻿/*Recebe os toques da tela*/

using UnityEngine;
using System.Collections;

public class ButtonBehaviour : MonoBehaviour {
	
	public Material normalButton;
	public Material pressButton;
	public Material overButton;
	public Renderer rendererButton;
	
	// Use this for initialization
	void Start () {
		rendererButton.material = normalButton;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick(){
		rendererButton.material = pressButton;
	}
	
	void OnMouseOver(){
		renderer.material = overButton;
	}
	void OnMouseExit(){
		rendererButton.material = normalButton;
	}
}
