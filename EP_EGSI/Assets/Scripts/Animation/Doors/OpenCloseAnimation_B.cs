﻿using UnityEngine;
using System.Collections;

public class OpenCloseAnimation_B : MonoBehaviour {

	public GameObject door1;
	public GameObject door2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider hit){
		if(hit.gameObject.tag == "Player"){
			door1.animation.Play("door_B1");
			door2.animation.Play("door_B2");
			
		}
	}
	
	void OnTriggerExit(Collider hit){
		if(hit.gameObject.tag == "Player"){
			door1.animation.Play("door_B1_back");
			door2.animation.Play("door_B2_back");
		}
	}
}
