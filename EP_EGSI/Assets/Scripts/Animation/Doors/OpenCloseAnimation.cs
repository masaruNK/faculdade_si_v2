﻿using UnityEngine;
using System.Collections;

public class OpenCloseAnimation : MonoBehaviour {

	public GameObject door1;
	public GameObject door2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider hit){
		if(hit.gameObject.tag == "Player"){
			door1.animation.Play("door_A1");
			door2.animation.Play("door_A2");

		}
	}

	void OnTriggerExit(Collider hit){
		if(hit.gameObject.tag == "Player"){
			door1.animation.Play("door_A1_back");
			door2.animation.Play("door_A2_back");
		}
	}
}
