﻿using UnityEngine;
using System.Collections;
using MySql.Data.MySqlClient;

public class PlayerMovement : MonoBehaviour {
	
	//public GameManager manager;
	//public Tempo manager2;
	public float moveSpeed;
	private float maxSpeed = 10f;
	private Vector3 input;
	private Vector3 positionStart;
	public GameObject deathParticles;
	public int totalLife = 3; //total de vida do player
	public Renderer[] hearts;
	//private int currentLife;

	//pisca quando perde uma vida
	private float timeToInvencibility = 1;
	private float currentTimeToInvencibility = 0;
	private float timeToBlink = 0.1f;
	private float currentTimeToBlink = 0;
	private bool invencibility = false;
	public Renderer rendererPlayer;

	public int coins = 0;
	public TextMesh totalCoins;
	public GameObject coinParticle;

	public AudioClip coin;
	public AudioClip lifeAudio;


	/********Variaveis GAME MANAGER************/

	//GUI skin
	public GUISkin skin;

	//Count
	public int currentScore;
	public int highScore;
	private int currentLevel = 2; 
	public int unlockLevel;
	private int levels = 4; //qtd de levels, fases
	
	//Timer variables
	private float startTime = 20f;
	private string currentTime;
	public Rect timerRect;
	public Color warningColorTime;
	public Color defaultColorTime;
	private float alertTime = 5f; //5 segundos para o fim, o GUI time ficara vermelho (warningTime)
	private bool catchToken = false; //quando pega o token o tempo volta aos 20 segundos
	
	//References
	//public GameObject tokenParent;
	
	//Outras variaveis
	private bool showWinScreen; //mostra a tela 
	public int winScreenWidth, winScreenHeight;

	/*****************************/
	public TextMesh startTime2;
	private float currentTime2;

	public bool completed;


	/***********DATABASE**********/
	private string source;
	private MySqlConnection conexao;

	private string usuario = "kmn1405";
	private string senha = "123456";

	
	void Awake(){
		//currentLife = totalLife;
	}

	// Use this for initialization
	void Start () {
		/*******Conexao BD********/
		source = 	"Server = localhost;"+
							"Database = unity;"+
							"User ID = root;"+
							"Password = 65zyKO;"+
							"Pooling = false;";
		
		ConectarBD(source);
		Read(conexao);
		/*********Fim BD*******/

		positionStart = transform.position;
		currentTime2 = 0f;
		//startTime.text = "20";
		startTime2.text = string.Format("{0:0.0}", currentTime2);
		/*manager = manager.GetComponent<GameManager>();
		manager2 = manager2.GetComponent<Tempo>();*/

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		positionStart = transform.position; //toda vez que o player for atingido ficara no mesmo lugar
		input = new Vector3(Input.GetAxisRaw("Horizontal"),0,Input.GetAxisRaw("Vertical"));

		if (!completed)GameTime();

		if(rigidbody.velocity.magnitude < maxSpeed){
			rigidbody.AddForce(input * moveSpeed);
		}

		if(transform.position.y < -1){ //verifica se o player caiu no infinito, saiu da area
			Die();
		}
		if(invencibility){
			Invencibility();
		}
	}

	/*******************BANCO DE DADOS*********************/
	void ConectarBD(string _source){
		conexao = new MySqlConnection(_source);
		conexao.Open();
		Debug.Log("Conectado!");
	}
	
	void Read(MySqlConnection _conexao){
		MySqlCommand command = _conexao.CreateCommand(); //instanciando o comando
		command.CommandText = "select*from login where username='kmn1405'";
		MySqlDataReader dados = command.ExecuteReader();
		
		while(dados.Read()){
			string username = (string)dados["username"];
			string password = (string)dados["password"];
			Debug.Log("Usuario: " + username + "\tPassword: " + password);
			
		}
	}
	/********************FIM BANCO DE DADOS******************/

	void Die(){
		if(!invencibility){
			Instantiate (deathParticles,transform.position,Quaternion.identity);
			transform.position = positionStart;
			LoseLife();
		}
	}

	void OnTriggerEnter(Collider other){ //other eh o objeto que nos iremos colidir

		if((other.gameObject.tag == "Enemy")||(other.gameObject.tag == "Enemy_Black")){
			Die();
			invencibility = true;
		}
		/*if(other.gameObject.tag == "Token"){
			//manager.AddToken();
			manager2.AddToken();
			Destroy(other.gameObject);
		}*/

		if(other.transform.tag == "Goal"){
			CompleteLevel();
		}
		if(other.transform.tag == "Moeda"){
			Destroy(other.gameObject);
			audio.PlayOneShot(coin);
			//Instantiate(coinParticle,transform.position,Quaternion.identity);
			coins++;
			totalCoins.text = coins.ToString();
		}
		if(other.gameObject.tag == "Enemy2_power"){
			Die();
			invencibility = true;
		}
		if(other.transform.tag == "Heart"){
			audio.PlayOneShot(lifeAudio);
			GetLife();
			Destroy(other.gameObject);
		}
	}

	void CallGameOver(){
		Application.LoadLevel(0); //*FIX -> colocar uma msg de CONTINUE
	}

	void LoseLife(){
		totalLife--;
		audio.PlayOneShot(lifeAudio);
		hearts[totalLife].enabled = false;
		//Debug.Log(totalLife);
		if(totalLife == 0){
			CallGameOver();
		}
	}

	//Add 1 vida ao encostar no coracao
	void GetLife(){
		if(totalLife < 3){
			totalLife++;
			hearts[totalLife-1].enabled = true; //lembra que eh vetor, comeca em zero
		}
	}

	//Invencibilidade temporaria ao apanhar	
	void Invencibility(){
		currentTimeToBlink += Time.deltaTime;
		if(currentTimeToBlink > timeToBlink){
			rendererPlayer.enabled = !rendererPlayer.enabled;
			currentTimeToBlink = 0;
		}
		currentTimeToInvencibility += Time.deltaTime; //contagem do tempo
		if(currentTimeToInvencibility > timeToInvencibility){
			invencibility = false;
			currentTimeToInvencibility = 0;
			rendererPlayer.enabled = true;
		}
	}

	/*****************GAME MANAGER****************/
	public void CompleteLevel(){
		showWinScreen = true;
		completed = true;

	}
	
	void LoadNextLevel(){
		if(currentLevel < levels){ //enquanto nao estiver na ultima fase, ira incrementar o currentLevel
			currentLevel++;
			print (currentLevel);
			SaveGame();
			Application.LoadLevel(currentLevel); 
		}else
			print("You win!");
	}
	

	void SaveGame(){
		PlayerPrefs.SetInt("Level Completed",currentLevel);
		PlayerPrefs.SetInt("Level " + currentLevel.ToString()+" score",currentLevel);
		PlayerPrefs.GetInt("Level Completed");
	}
	
	//todo GUI deve estar no metodo OnGUI ? -> pesquisar
	void OnGUI(){
		GUI.skin = skin;
		if(showWinScreen){
			Rect winScreenRect = new Rect(Screen.width/2 - (Screen.width*.4f/2), Screen.height/2 - (Screen.height*.4f/2),Screen.width*.4f,Screen.height*.4f);
			GUI.Box(winScreenRect,"LEVEL FINALIZADO!");
			
			if(GUI.Button(new Rect(winScreenRect.x + winScreenRect.width -170 , winScreenRect.y + winScreenRect.height -60, 150, 40), "Continue")){
				LoadNextLevel();
			}
			
			if(GUI.Button(new Rect(winScreenRect.x + 20 , winScreenRect.y + winScreenRect.height -60, 150, 40), "Quit")){
				Application.LoadLevel("menu");
			}
			
			GUI.Label(new Rect(winScreenRect.x + 20, winScreenRect.y + 40, 300, 50),"Moedas coletadas: \t"+ totalCoins.text);
			GUI.Label(new Rect(winScreenRect.x + 20, winScreenRect.y + 90, 300, 50),"Tempo: \t"+ startTime2.text);
			GUI.Label(new Rect(winScreenRect.x + 20, winScreenRect.y + 140, 300, 50),"Total de Pontos: \t"+ Score ());
		}
	}


	/******************************/
	public void GameTime(){
		
		startTime2.color = Color.white;
		currentTime2 += Time.deltaTime;
		startTime2.text = string.Format("{0:0.0}", currentTime2);
		
	}

	public string Score(){
		float score = Mathf.Round((coins*5) - ((float)0.2*currentTime2));
		string scoreText = score.ToString();
		return scoreText;
	}
	
}
